export const URL_GATEWAY = process.env.URL_GATEWAY || "http://apigateway.toedu.me"
export const URL_SERVER = process.env.URL_SERVER || "http://toedu.me/"
export const API_SERVER = process.env.BASE_URL || "http://localhost:1900"