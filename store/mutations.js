export const setInfoAuth = (state, payload) => {
  state.token = payload.token ? payload.token : "";
  state.username = payload.username ? payload.username : "";
  state.sid = payload.sid ? payload.sid : "";
};

export const setToken = (state, token) => {
  state.token = token;
};

export const setUsername = (state, username) => {
  state.username = username;
};