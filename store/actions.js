export async function nuxtServerInit({ commit, dispatch }) {
  let token = this.$cookiz.get("x-token") || null;
  let username = this.$cookiz.get("username") || null;
  let sid = this.$cookiz.get("sid") || null;
  if (token) {
    commit("setInfoAuth", {
      token,
      username,
      sid
    });
  }
}

export async function setToken({commit}, payload){
  this.$cookiz.set("x-token", payload);
  commit("setToken", payload);
}

export async function setUsername({commit}, payload){
  this.$cookiz.set("username", payload);
  commit("setUsername", payload);
}

export async function saveAuth({ commit }, payload) {
  try {
    this.$cookiz.set("x-token", payload.token);
    this.$cookiz.set("username", payload.username);
    this.$cookiz.set("sid", payload.sid);
    commit("setInfoAuth", payload);
  } catch (e) {
    console.log({ e });
  }
}

export async function removeAuth({ commit }) {
  this.$cookiz.set("x-token", null);
  this.$cookiz.set("username", null);
  this.$cookiz.set("sid", null);
  commit("setInfoAuth", {});
}
