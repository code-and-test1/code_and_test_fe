let convertDate = (strDate, strTime) => {
  if (strDate.length <= 0 || strTime.length <= 0) {
    return "";
  }
  var arrDateStart = strDate.split("-");
  var arrTimeStart = strTime.split(":");
  return new Date(
    arrDateStart[0],
    arrDateStart[1] - 1,
    arrDateStart[2],
    arrTimeStart[0],
    arrTimeStart[1],
    arrTimeStart[2]
  );
};

let createFile = (data, filename, type) => {
  var file = new Blob([data], { type: type });
  return file;
  // return new File(file, "filename");
  // Others
  var a = document.createElement("a"),
    url = URL.createObjectURL(file);
  a.href = url;
  a.download = filename;
  document.body.appendChild(a);
  a.click();
  setTimeout(function() {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }, 0);
};

let convertFulldate = date => {
  if (typeof date == "string") {
    date = new Date(date);
  }
  return (
    date.getHours() +
    ":" +
    date.getMinutes() +
    ":" +
    date.getSeconds() +
    " " +
    date.getDate() +
    "/" +
    (date.getMonth() + 1) +
    "/" +
    date.getFullYear()
  );
};

let dateToString = strDate => {
  let date = new Date(strDate);
  return (
    date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
  );
};

let dateToTime = date => {
  return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
};

export default () => ({
  convertDate,
  convertFulldate,
  dateToString,
  dateToTime,
  createFile
});
