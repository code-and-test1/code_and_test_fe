import { URL_SERVER } from "../contants/api";

const isRedirectedFromMainaApp = (url, sid) => {
    if (url.indexOf("/redirect") >= 0 && sid !== null && sid !== "") {
        return true;
    }
    return false;
}

const isGoToAdmin = async (store, token) => {
    try {
        let data = await store.$repositoryFactory.authen.GetUser({ token: token });
        if(data.data != null){
            store.dispatch("setUsername", data.data.data.name);
        }
        if (data.data.code == 200 && data.data.data != null && data.data.data.listRoles.indexOf("ADMIN") >= 0) {
            return true;
        }
    } catch (err) {
        console.log("IsGotoAdmin error: ", err);
    }
    return false;
}

const setToken = (store, token) => {
    return store.dispatch("setToken", token);
}

export default async function ({ store, redirect, route }) {
    let sid = route.query.sid;
    let url = route.path
    if (isRedirectedFromMainaApp(url, route.query.sid)) {
        // lấy về Token từ sid
        try {
            let result = await store.$repositoryFactory.authen
                .getTokenFromG12({ sid: sid })
            if (result.code == 401) {
                // sid không hợp lệ
                console.log("Code 401 with result: ", result);
                return redirect(URL_SERVER)
            } else if (result.code == 200) {
                // Lấy token Thành công
                await setToken(store, result.data.token);
                let isAdmin = await isGoToAdmin(store, result.data.token);
                if (isAdmin) {
                    redirect("/admin");
                }else{
                    redirect("/");
                }
            } else {
                // Lỗi khác
                console.log("Error");
                return redirect(URL_SERVER)
            }
        } catch (err) {
            // Có lỗi xảy ra
            console.log(err);
            return redirect(URL_SERVER)
        };
    } else {
        // Không phải redirect từ Main App, kiểm tra có token không
        let token = store.state.token;
        if (token != null && typeof token != undefined && token != "") {
            // Có token
            // Gửi request check Token
            try {
                let result = await store.$repositoryFactory.authen
                    .checkTokenInG12({ token: token })
                if (result.status != 200) {
                    return redirect(URL_SERVER)
                } else {
                    let isAdmin = await isGoToAdmin(store, token);
                    if (isAdmin && url == "/") {
                        redirect("/admin");
                    }else if(!isAdmin && url == "/admin"){
                        redirect("/");
                    }
                }
            } catch (err) {
                // Có lỗi xảy ra
                console.log(err)
                return redirect(URL_SERVER)
            };
        } else {
            // Trường hợp không có token, redirect qua main app
            console.log("None redirect from app, none token");
            return redirect(URL_SERVER)
        }
    }
}
