export default async function({ store, redirect, route }) {
  const tokenBearer = store.state.admin.auth.token;
  if (!tokenBearer && route.fullPath != "/login") {
    return redirect("/admin/login");
  }
}
