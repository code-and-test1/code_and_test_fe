export default async function({ store, redirect, route }) {
  const tokenBearer = store.state.token;
  if (
    !tokenBearer &&
    route.fullPath != "/register" &&
    route.fullPath != "/login"
  ) {
    return redirect("/login");
  }
}
