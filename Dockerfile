# for build
FROM node:13.7.0-alpine as builder

RUN apk update && apk add yarn
RUN mkdir /app

WORKDIR /app
COPY . /app
RUN apk update && apk add --no-cache --virtual .gyp python make g++

ENV NODE_ENV production
ENV BASE_URL http://apigateway.toedu.me/nhom4/api 
ENV BROWSER_BASE_URL http://codeatest.cf
ENV URL_GATEWAY http://apigateway.toedu.me 
ENV URL_SERVER http://toedu.me 
RUN yarn install --production=true && yarn build

# for RUN

FROM node:13.7.0-alpine

ENV NODE_ENV production
ENV BASE_URL http://apigateway.toedu.me/nhom4/api 
ENV BROWSER_BASE_URL http://codeatest.cf
ENV URL_GATEWAY http://apigateway.toedu.me 
ENV URL_SERVER http://toedu.me 
ENV NUXT_HOST 0.0.0.0
ENV NUXT_PORT 3000
WORKDIR /app
# COPY ./docker-entrypoint-prod.sh /app
# RUN chmod +x /app/docker-entrypoint-prod.sh
COPY --from=builder /app/.nuxt .nuxt
COPY --from=builder /app/node_modules node_modules
COPY --from=builder /app/static static
COPY ./package.json /app/package.json

RUN npm prune
EXPOSE 3000
CMD [ "npm", "start" ]
