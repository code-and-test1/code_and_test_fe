import data from "~/contants/data";
export default $axios => ({
  async get(payload) {
    let data = await $axios.$get(`/quizzes/${payload.id}`);
    return data;
  },
  async getByTopic(payload) {
    let data = await $axios.$get(`/topics/${payload.id}/quizzes`);
    return data;
  },
  getByTopicAUserId(payload) {
    return {
      data: data.listQuiz
    };
  },

  async edit(payload) {
    let data = await $axios.$put(`/quizzes/${payload.id}`, payload);
    return data;
  },

  async rankingByQuiz(payload) {
    let data = await $axios.$get(`/quizzes/${payload.id}/ranking`);
    return data;
  },

  async add(payload) {
    let data = await $axios.$post(`/quizzes`, Object.assign({}, payload));
    return data;
  },
  async delete(payload) {
    let data = await $axios.$delete(`/quizzes/${payload.id}`, payload);
    return data;
  },

  async join(payload) {
    let data = await $axios.post(`/quizzes/${payload.quizId}/join`);
    return data;
  },

  async getTestCase(payload) {
    let data = await $axios.$get(`/test-cases`, { id: payload.id });
    return data;
  },

  async createTestCase(payload) {
    let data = await $axios.$post(`/quizzes/${payload.id}/test-cases`, payload);
    return data;
  },

  async editTestCase(payload) {
    let data = await $axios.$put(
      `/test-cases/${payload.test_case_id}`,
      payload.testCase
    );
    return data;
  },

  async deleteTestCase(payload) {
    let data = await $axios.$delete(`/test-cases/${payload._id}`);
    return data;
  },

  async statistic(payload) {
    let data = await $axios.$get(
      `/statistic/quiz/count-submissions/?quizId=${payload.id}&year=${payload.year}`
    );
    return data;
  },

  async getLanguage(){
    let data = await $axios.$get(
      `/workers/supported-langs`
    );
    return data;
  }
});
