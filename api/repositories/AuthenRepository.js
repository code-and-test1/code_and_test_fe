import data from "~/contants/data";
import { URL_GATEWAY, API_SERVER } from "~/contants/api";
export default $axios => ({
  authenToBackend(payload) {
    var reqContentDemo = {
      method: "get",
      url: `${API_SERVER}/api/`,
      headers: {
        Authorization: payload.token,
        "Content-Type": "application/json",
      },
    };
    return $axios(reqContentDemo)
  },

  getTokenFromG12(payload) {
    let data = $axios.$get(`${URL_GATEWAY}/auth/api/intergrates/token?sid=${payload.sid}`)
    return data;
  },

  checkTokenInG12(payload) {
    var reqContent = {
      method: "get",
      url: `${URL_GATEWAY}/auth/api/intergrates/authen`,
      headers: {
        Authorization: payload.token,
        "Content-Type": "application/json",
      },
    };
    return $axios(reqContent)
  },

  GetUser(payload){
    var reqContent = {
      method: "get",
      url: `${URL_GATEWAY}/auth/api/intergrates/users/me`,
      headers: {
        Authorization: payload.token,
        "Content-Type": "application/json",
      },
    };
    return $axios(reqContent)
  }
})