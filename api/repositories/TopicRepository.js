import data from "~/contants/data";
export default $axios => ({
  async get() {
    let data = await $axios.$get("/topics");
    return data;
  },
  async getById(payload) {
    let data = await $axios.$get(`/topics/${payload.id}`);
    return {
      data
    };
  },
  async getTopic(payload) {
    let data = await $axios.$get(`/topics/${payload.slug}`);
    return data;
  },
  getMember(payload) {
    return 10;
  },
  getByCategoryAUserId() {
    return {
      data: data.listTopic
    };
  },
  async statistic(payload) {
    let data = await $axios.$get(
      `/statistic/topic/count-submissions/?topicId=${payload.id}&year=${payload.year}`
    );
    return data;
  },
  async add(payload) {
    let data = await $axios.$post(`/topics`, payload);
    return data;
  },
  async edit(payload) {
    let data = await $axios.$put(`/topics/${payload._id}`, payload);
    return data;
  },

  async rankingByTopic(payload) {
    let data = await $axios.$get(`/topics/${payload.id}/ranking`);
    return data;
  },

  async delete(payload) {
    let data = await $axios.$delete(`/topics/${payload.id}`);
    return data;
  },

  async join(payload) {
    let data = await $axios.post(`/topics/${payload.topicId}/join`);
    return data;
  }
});
