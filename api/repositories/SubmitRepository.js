export default $axios => ({
  async get(payload) {
    let data = await $axios.$get("/submits", { token: payload.token });
    return data;
  },

  async getByUser(payload) {
    let data = await $axios.$get(`/me/submissions`);
    return data;
  },

  async submit(payload) {
    let data = await $axios.$post(
      `/quizzes/${payload.quiz_id}/submission`,
      payload.formData,
      {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }
    );
    return data;
  },

  async getByTopic(payload) {
    let data = await $axios.$get(
      `/statistic/topic/count-submissions/?topicId=${payload.id}`
    );
    return data;
  },

  async getByQuiz(payload) {
    let data = await $axios.$get(
      `/me/quizzes/${payload.id}/submissions`
    );
    return data;
  },

  async getJob(payload) {
    let data = await $axios.$get(
      `/submits/${payload.id}/jobs?status=done`
    );
    return data;
  },
});
