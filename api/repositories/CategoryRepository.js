export default $axios => ({
  async get(context) {
    let data = await $axios.$get("/categories");
    return {
      data: data.data.categories,
      success: true
    };
  },

  async getById(payload) {
    let data = await $axios.$get(`/categories/${payload.id}`);
    return data;
  },

  async add(payload) {
    let data = await $axios.$post("/categories", payload);
    return data;
  },

  async edit(payload) {
    let data = await $axios.$put(`/categories/${payload.id}`, payload);
    return data;
  },

  async delete(payload) {
    let data = await $axios.$delete(`/categories/${payload.id}`, payload);
    return data;
  }
});
