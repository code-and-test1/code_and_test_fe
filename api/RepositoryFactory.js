import CategoryRepository from "./repositories/CategoryRepository";
import TopicRepository from "./repositories/TopicRepository";
import QuizRepository from "./repositories/QuizRepository";
import UserRepository from "./repositories/UserRepository";
import SubmitRepository from "./repositories/SubmitRepository";
import AuthenRepository from "./repositories/AuthenRepository"

export default $axios => ({
  category: CategoryRepository($axios),
  topic: TopicRepository($axios),
  quiz: QuizRepository($axios),
  user: UserRepository($axios),
  submit: SubmitRepository($axios),
  authen: AuthenRepository($axios)
});
