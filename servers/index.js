const app = require("express")();

// app.get('/', (req, res) => res.send(`Hi! I'm codeatest-fe`))
app.get("/ping", (req, res) => res.send(`codeatest-fe:pong`));

app.get("/healthz", (req, res) => res.send(`codeatest-fe healthz`));
app.get("/health", (req, res) => res.send(`codeatest-fe health`));

module.exports = app;
