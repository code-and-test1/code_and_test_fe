export default {
  listContest: [
    {
      id: 1,
      name: "Test thực tập sinh php",
      startTime: new Date(),
      idQuiz: "2",
      endTime: new Date("12/12/2020"),
      password: "1",
      status: "pending",
      member: [],
      create_at: new Date("1/1/2020")
    },
    {
      id: 2,
      name: "Test thực tập sinh Nodejs",
      idQuiz: "1",
      startTime: new Date(),
      endTime: new Date("12/12/2020"),
      password: "1",
      status: "pending",
      member: ["adfasdf", "3412341"],
      create_at: new Date("1/1/2020")
    },
    {
      id: 3,
      name: "Test 2",
      idQuiz: "2",
      startTime: new Date(),
      endTime: new Date("12/12/2020"),
      password: "",
      status: "finish",
      member: ["sdfasdf", "324234"],
      create_at: new Date("1/1/2021")
    },
    {
      id: 4,
      name: "Test 3",
      idQuiz: "3",
      startTime: new Date(),
      endTime: new Date("12/12/2020"),
      password: "",
      status: "happening",
      member: [],
      create_at: new Date("1/1/2020")
    }
  ],
  listCategory: [
    {
      name: "My progress",
      id: 1
    },
    {
      name: "Topic",
      id: 2
    },
    {
      name: "Contest",
      id: 3
    }
  ],
  listTopic: [
    {
      id: 1,
      name: "Cấu trúc dữ liệu và giải thuật",
      category: 2,
      progress: 40,
      image:
        "https://ifactory.com.vn/wp-content/uploads/201    12/Data-technology-1.jpg",
      description: "Đây là miêu tả của cấu trúc dữ liệu và giải thuật",
      status: "active",
      updated_at: new Date(),
      expired_at: new Date()
    },
    {
      id: 2,
      name: "AI",
      category: 2,
      progress: 90,
      image:
        "https://ifactory.com.vn/wp-content/uploads/201    12/Data-technology-1.jpg",
      description: "This is description of cau truc du lieu va giair thuat",
      updated_at: new Date(),
      expired_at: new Date()
    },
    {
      id: 3,
      name: "Machine learning",
      category: 2,
      progress: 40,
      image:
        "https://ifactory.com.vn/wp-content/uploads/201    12/Data-technology-1.jpg",
      description: "Đây là miêu tả của cấu trúc dữ liệu và giải thuật",
      updated_at: new Date(),
      expired_at: new Date()
    },
    {
      id: 4,
      name: "Data scient",
      category: 2,
      progress: 10,
      image:
        "https://ifactory.com.vn/wp-content/uploads/201    12/Data-technology-1.jpg",
      description: "Đây là miêu tả của cấu trúc dữ liệu và giải thuật",
      updated_at: new Date(),
      expired_at: new Date()
    },
    {
      id: 5,
      name: "Kho dữ liệu",
      category: 2,
      image:
        "https://ifactory.com.vn/wp-content/uploads/201    12/Data-technology-1.jpg",
      description: "Đây là miêu tả của Thi 1",
      updated_at: new Date(),
      expired_at: new Date()
    },
    {
      id: 6,
      name: "Cài win",
      category: 2,
      image:
        "https://ifactory.com.vn/wp-content/uploads/201    12/Data-technology-1.jpg",
      description: "Đây là miêu tả của Thi 2",
      updated_at: new Date(),
      expired_at: new Date()
    }
  ],
  listQuiz: [
    {
      id: 1,
      name: "Câu hỏi 1",
      description: "Đây là câu hỏi đầu tiên trong bộ câu hỏi",
      content:
        "Đây là nội dung của câu hỏi 1, Đây là nội dungcủa câu hỏi 1, Đây là nội dung của câu hỏi 1, Đây là nộidung của câu hỏi 1, Đây là nội dung của câu hỏi 1,Đây là nội dung của câu hỏi 1, Đây là nội dungcủa câu hỏi 1, Đây là nội dung của câu hỏi 1, Đây là nộidung của câu hỏi 1, Đây là nội dung của câu hỏi 1,Đây là nội dung của câu hỏi 1, Đây là nội dungcủa câu hỏi 1, Đây là nội dung của câu hỏi 1, Đây là nộidung của câu hỏi 1, Đây là nội dung của câu hỏi 1,Đây là nội dung của câu hỏi 1, Đây là nội dungcủa câu hỏi 1, Đây là nội dung của câu hỏi 1, Đây là nộidung của câu hỏi 1, Đây là nội dung của câu hỏi 1,Đây là nội dung của câu hỏi 1, Đây là nội dungcủa câu hỏi 1, Đây là nội dung của câu hỏi 1, Đây là nộidung của câu hỏi 1, Đây là nội dung của câu hỏi 1,Đây là nội dung của câu hỏi 1, Đây là nội dungcủa câu hỏi 1, Đây là nội dung của câu hỏi 1, Đây là nộidung của câu hỏi 1, Đây là nội dung của câu hỏi 1,",
      topic: {
        id: 1,
        name: "dsfasfsa"
      },
      stat: {
        difficult: "easy",
        author: "Alex",
        score: 1
      },
      limit: {
        timeout: 150
      },
      updated_at: new Date(),
      answer: "THis is answer, this is answer"
    },
    {
      id: 2,
      name: "Câu hỏi 2",
      description: "Đây là câu hỏi đầu tiên trong bộ câu hỏi",
      content:
        "Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,Đây là nội dung của câu hỏi 2,",
      topic: {
        id: 1,
        name: "dsfasfsa"
      },
      stat: {
        difficult: "easy",
        author: "Alex",
        score: 1
      },
      limit: {
        timeout: 150
      },
      updated_at: new Date(),
      answer: "THis is answer, this is answer"
    },
    {
      id: 3,
      name: "Câu hỏi 3",
      description: "Đây là câu hỏi đầu tiên trong bộ câu hỏi",
      content:
        "Đây là nội dung của câu hỏi 3, Đây là nội dungcủa câu hỏi 3, Đây là nội dung của câu hỏi 3, Đây là nộidung của câu hỏi 3, Đây là nội dung của câu hỏi 3,Đây là nội dung của câu hỏi 3, Đây là nội dungcủa câu hỏi 3, Đây là nội dung của câu hỏi 3, Đây là nộidung của câu hỏi 3, Đây là nội dung của câu hỏi 3,Đây là nội dung của câu hỏi 3, Đây là nội dungcủa câu hỏi 3, Đây là nội dung của câu hỏi 3, Đây là nộidung của câu hỏi 3, Đây là nội dung của câu hỏi 3,Đây là nội dung của câu hỏi 3, Đây là nội dungcủa câu hỏi 3, Đây là nội dung của câu hỏi 3, Đây là nộidung của câu hỏi 3, Đây là nội dung của câu hỏi 3,Đây là nội dung của câu hỏi 3, Đây là nội dungcủa câu hỏi 3, Đây là nội dung của câu hỏi 3, Đây là nộidung của câu hỏi 3, Đây là nội dung của câu hỏi 3,Đây là nội dung của câu hỏi 3, Đây là nội dungcủa câu hỏi 3, Đây là nội dung của câu hỏi 3, Đây là nộidung của câu hỏi 3, Đây là nội dung của câu hỏi 3,",
      topic: {
        id: 1,
        name: "dsfasfsa"
      },
      stat: {
        difficult: "medium",
        author: "Alex",
        score: 1
      },
      updated_at: new Date(),
      limit: {
        timeout: 150
      },
      answer: "THis is answer, this is answer"
    },
    {
      id: 4,
      name: "Câu hỏi 4",
      description: "Đây là câu hỏi đầu tiên trong bộ câu hỏi",
      content:
        "Đây là nội dung của câu hỏi 4, Đây là nội dungcủa câu hỏi 4, Đây là nội dung của câu hỏi 4, Đây là nộidung của câu hỏi 4, Đây là nội dung của câu hỏi 4,Đây là nội dung của câu hỏi 4, Đây là nội dungcủa câu hỏi 4, Đây là nội dung của câu hỏi 4, Đây là nộidung của câu hỏi 4, Đây là nội dung của câu hỏi 4,Đây là nội dung của câu hỏi 4, Đây là nội dungcủa câu hỏi 4, Đây là nội dung của câu hỏi 4, Đây là nộidung của câu hỏi 4, Đây là nội dung của câu hỏi 4,Đây là nội dung của câu hỏi 4, Đây là nội dungcủa câu hỏi 4, Đây là nội dung của câu hỏi 4, Đây là nộidung của câu hỏi 4, Đây là nội dung của câu hỏi 4,Đây là nội dung của câu hỏi 4, Đây là nội dungcủa câu hỏi 4, Đây là nội dung của câu hỏi 4, Đây là nộidung của câu hỏi 4, Đây là nội dung của câu hỏi 4,Đây là nội dung của câu hỏi 4, Đây là nội dungcủa câu hỏi 4, Đây là nội dung của câu hỏi 4, Đây là nộidung của câu hỏi 4, Đây là nội dung của câu hỏi 4,",
      topic: {
        id: 1,
        name: "dsfasfsa"
      },
      stat: {
        difficult: "easy",
        author: "Alex",
        score: 1
      },
      updated_at: new Date(),
      limit: {
        timeout: 150
      },
      answer: "THis is answer, this is answer"
    },
    {
      id: 5,
      name: "Câu hỏi 5",
      description: "Đây là câu hỏi đầu tiên trong bộ câu hỏi",
      content:
        "Đây là nội dung của câu hỏi 5, Đây là nội dungcủa câu hỏi 5, Đây là nội dung của câu hỏi 5, Đây là nộidung của câu hỏi 5, Đây là nội dung của câu hỏi 5,Đây là nội dung của câu hỏi 5, Đây là nội dungcủa câu hỏi 5, Đây là nội dung của câu hỏi 5, Đây là nộidung của câu hỏi 5, Đây là nội dung của câu hỏi 5,Đây là nội dung của câu hỏi 5, Đây là nội dungcủa câu hỏi 5, Đây là nội dung của câu hỏi 5, Đây là nộidung của câu hỏi 5, Đây là nội dung của câu hỏi 5,Đây là nội dung của câu hỏi 5, Đây là nội dungcủa câu hỏi 5, Đây là nội dung của câu hỏi 5, Đây là nộidung của câu hỏi 5, Đây là nội dung của câu hỏi 5,Đây là nội dung của câu hỏi 5, Đây là nội dungcủa câu hỏi 5, Đây là nội dung của câu hỏi 5, Đây là nộidung của câu hỏi 5, Đây là nội dung của câu hỏi 5,Đây là nội dung của câu hỏi 5, Đây là nội dungcủa câu hỏi 5, Đây là nội dung của câu hỏi 5, Đây là nộidung của câu hỏi 5, Đây là nội dung của câu hỏi 5,",
      topic: {
        id: 2,
        name: "dsfasfsa"
      },
      updated_at: new Date(),
      stat: {
        difficult: "hard",
        author: "Alex",
        score: 1
      },
      limit: {
        timeout: 150
      },
      answer: "THis is answer, this is answer"
    }
  ]
};

// {
//     3: [
//       {
//         id: 1,
//         name: "Cấu trúc dữ liệu và giải thuật",
//         info: "info of topic nenenene"
//       },
//       {
//         id: 2,
//         name: "Cấu trúc dữ liệu và giải thuật",
//         info: "info of topic nenenene"
//       },
//       {
//         id: 3,
//         name: "Cấu trúc dữ liệu và giải thuật",
//         info: "info of topic nenenene"
//       },
//       {
//         id: 4,
//         name: "Cấu trúc dữ liệu và giải thuật",
//         info: "info of topic nenenene"
//       },
//       {
//         id: 2,
//         name: "Cấu trúc dữ liệu và giải thuật",
//         info: "info of topic nenenene"
//       },
//       {
//         id: 3,
//         name: "Cấu trúc dữ liệu và giải thuật",
//         info: "info of topic nenenene"
//       },
//       {
//         id: 4,
//         name: "Cấu trúc dữ liệu và giải thuật",
//         info: "info of topic nenenene"
//       },
//     ],

// 1: [
//     {
//       id: 1,
//       name: "Cấu trúc dữ liệu và giải thuật",
//       info: "info of topic nenenene",
//       progress: 40,
//     },
//     {
//       id: 2,
//       name: "Cấu trúc dữ liệu và giải thuật",
//       info: "info of topic nenenene",
//       progress: 30,
//     },
//     {
//       id: 3,
//       name: "Cấu trúc dữ liệu và giải thuật",
//       info: "info of topic nenenene",
//       progress: 90,
//     },
//   ]
