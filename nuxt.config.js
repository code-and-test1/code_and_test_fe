export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: "nuxt_app",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href:
          "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css"
      }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    "bootstrap/dist/css/bootstrap.css",
    "bootstrap-vue/dist/bootstrap-vue.css",
    /* Import Font Awesome Icons Set */
    "~/node_modules/flag-icon-css/css/flag-icon.min.css",
    /* Import Font Awesome Icons Set */
    "~/node_modules/font-awesome/css/font-awesome.min.css",
    "~/node_modules/codemirror/lib/codemirror.css",
    // merge css
    "~/node_modules/codemirror/addon/merge/merge.css",
    // theme css
    "~/node_modules/codemirror/theme/base16-dark.css"
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: "~/plugins/repositories", ssr: true },
    { src: "~/plugins/axios", ssr: true },
    { src: "~/plugins/helper", ssr: true },
    { src: "~/plugins/vee-validate", ssr: false },
    { src: "~plugins/nuxt-codemirror-plugin.js", ssr: false }
  ],

  axios: {
    baseURL: process.env.BASE_URL || "http://localhost:1900"
    // baseURL: "http://codeatest.cf/api/"
    // baseURL: "http://localhost:1900"
  },

  publicRuntimeConfig: {
    axios: {
      browserBaseURL: process.env.BROWSER_BASE_URL || ""
    }
  },

  privateRuntimeConfig: {
    axios: {
      baseURL: process.env.BASE_URL
    }
  },

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    "bootstrap-vue/nuxt",
    "@nuxtjs/axios",
    ["cookie-universal-nuxt", { alias: "cookiz" }],
    "@nuxtjs/auth-next"
  ],

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},
  serverMiddleware: [{ path: "/", handler: "~/servers" }]
};
