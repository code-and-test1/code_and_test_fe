// import Notify from '../mixins/notify'

export default function ({app, redirect, store, error, route}) {
  app.$axios.onRequest(config => {
    const bearToken = store.getters['getToken']
    bearToken && (config.headers['Authorization'] = bearToken)
    return config
  })

  app.$axios.onResponse(response => {
    // Do something...
  })
}
