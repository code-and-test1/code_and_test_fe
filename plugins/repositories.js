import repositoryFactory from '~/api/RepositoryFactory'

export default ({ $axios }, inject) => {
  inject('repositoryFactory', repositoryFactory($axios));
};
