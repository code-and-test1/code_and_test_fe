import helper from '~/helper/index'

export default (context, inject) => {
  inject('helper', helper());
};
